#! /usr/bin/python3
import argparse, sys, math, re


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('ip_expr', nargs='?',
						help="input expression to be evaluated")
	parser.add_argument('-f', type=int, metavar="precision",
						help="specify floating point output precision")
	parser.add_argument('-F', action="store_true",
						help="turn off default precision of .2f")
	parser.add_argument('-b', action="store_true",
						help="output in binary format")
	parser.add_argument('-o', action="store_true",
						help="output in octal format")
	parser.add_argument('-x', action="store_true",
						help="output in hexadecimal format")
	parser.add_argument('-v', action="store_true",
						help="verbose mode, shows both input and output")

	args = parser.parse_args()

	if args.ip_expr in (None, '-'):
		args.ip_expr = sys.stdin.readline().strip()

	math_expressions = re.findall("[a-zA-Z]+|[-]?[0-9]+[.]?[0-9]*[!]", args.ip_expr)
	for expr in math_expressions:
		if expr.endswith("!"):
			num = expr[:len(expr)-1]
			try:
				num = int(num)
			except (ValueError):
				sys.exit("Error: Factorial numbers should be integers")
			try:
				assert num >= 0
			except (AssertionError):
				sys.exit("Error: Factorial numbers should be positive")
			args.ip_expr = args.ip_expr.replace(expr, "math.factorial(" + str(num) + ")")
		else:
			args.ip_expr = args.ip_expr.replace(expr, "math." + expr)

	try:
		result = eval(args.ip_expr)
		
		if isinstance(result, float):
			if result.is_integer():
				result = int(result)
		
		if args.f and isinstance(result, float):
			result = f'{result:.{args.f}f}'
		elif not args.F and isinstance(result, float):
			result = f'{result:.2f}'
		elif args.b:
			result = f'{int(result):#b}'
		elif args.o:
			result = f'{int(result):#o}'
		elif args.x:
			result = f'{int(result):#x}'

		if args.v:
			print(f'{args.ip_expr} = result')
		else:
			print(result)
	except (NameError, SyntaxError):
		sys.exit("Error: Not a valid input expression")
		

if __name__ == "main":
	main()
