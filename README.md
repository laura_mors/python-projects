# Practice Python Projects

Python projects based on the [Practice Python Projects](https://learnbyexample.github.io/practice_python_projects/cover.html) book.

I implemented most of the exercises suggested at the end of each chapter.