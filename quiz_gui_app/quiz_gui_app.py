import tkinter as tk
from tkinter import ttk
import random

class Root(tk.Tk):
    def __init__(self, question_blocks):
        super().__init__()

        self.question_blocks = question_blocks
        self.q_total = len(self.question_blocks)
        self.q_count = 1
        self.a_count = 0
        self.title('Multiple Choice Questions')
        self.geometry('400x300')
        self.iconbitmap('quiz_icon_small.ico')
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.create_frame()

    def create_frame(self):
        self.frame = tk.Frame()

        self.l_ask = tk.Label(self.frame, wraplength=300, justify='left',
        fg='brown', pady=10, font='TkFixedFont')
        self.l_ask.grid(column=0, row=0, columnspan=2)

        self.answer_count = self.check_answer_count()

        if self.answer_count > 1:
            self.create_checkboxes()
        else:
            self.create_radio()

        self.l_info = tk.Label(self.frame, pady=10)
        self.l_info.grid(column=0, row=5, columnspan=2, pady=5)

        self.b_submit = tk.Button(self.frame, text='Submit',
        state='disabled', command=self.submit )
        self.b_submit.grid(column=0, row=6)
        self.submit_clicked = False

        self.b_next = tk.Button(self.frame, text='Next',
        state='disabled', command=self.next)
        self.b_next.grid(column=1, row=6)

        self.user_input = []

        self.frame.pack(expand=True)
    
    def radio(self):
        if not self.submit_clicked:
            self.b_submit['state'] = 'normal'
    
    def create_radio(self):
        self.radio_choice = tk.IntVar()
        self.radio_choice.set(0)
        question, *choices = self.question_blocks[self.q_count-1].split('\n')
        random.shuffle(choices)
        self.l_ask['text'] = f'{self.q_count}) {question[question.find(" ")+1:]}'
        for idx, self.choice in enumerate(choices, 1):
            if self.choice.startswith('--> '):
                self.choice = self.choice[4:]
                self.answer = idx
            self.choice = self.choice[self.choice.find(" ")+1:]
            tk.Radiobutton(self.frame, text=self.choice, font='TkFixedFont',
            padx=20, variable=self.radio_choice, value=idx,
            command=self.radio).grid(column=0, row=idx, columnspan=2, sticky=tk.W, pady=5)
    
    
    def create_checkboxes(self):
        question, *choices = self.question_blocks[self.q_count-1].split('\n')
        random.shuffle(choices)
        self.l_ask['text'] = f'{self.q_count}) {question[question.find(" ")+1:]}'
        self.checkbox_choices = []
        for choice in choices:
            cb_var = tk.IntVar()
            cb_var.set(0)
            self.checkbox_choices.append(cb_var)
        self.answers = []
        for idx, self.choice in enumerate(choices, 1):
            if self.choice.startswith('--> '):
                self.choice = self.choice[4:]
                self.answers.append(idx)
            self.choice = self.choice[self.choice.find(" ")+1:]
            tk.Checkbutton(self.frame, text=self.choice, font='TkFixedFont',
            padx=20, variable=self.checkbox_choices[idx-1], onvalue=idx,
            command=self.radio).grid(column=0, row=idx, columnspan=2, sticky=tk.W, pady=5)
        
    
    def check_answer_count(self):
        _, *choices = self.question_blocks[self.q_count-1].split('\n')
        answer_count = 0
        for choice in choices:
            if choice.startswith('--> '):
                answer_count += 1
        
        return answer_count
    
    def submit(self):
        self.submit_clicked = True
        if self.answer_count > 1:
            user_input = [cb_choice.get() for cb_choice in self.checkbox_choices if cb_choice.get()]
            correct_answer = self.answers
        else:
            user_input = self.radio_choice.get()
            correct_answer = self.answer
        if user_input == correct_answer:
            self.a_count += 1
            self.l_info['fg'] = 'green'
            self.l_info['text'] = 'Correct answer! \U0001F44D'
        else:
            self.l_info['fg'] = 'red'
            self.l_info['text'] = ('\u274E Ooops! '
                                    f'The right choice is: {correct_answer}')
        self.b_submit['state'] = 'disabled'
        self.b_next['state'] = 'normal'

    def next(self):
        self.frame.destroy()
        self.q_count += 1
        if self.q_count <= self.q_total:
            self.create_frame()
        else:
            self.frame = tk.Frame()

            s = ttk.Style()
            s.configure("Report.TLabel", font="helvetica 16", foreground="#CB6266", background="#85D39C")
            report = f'You answered {self.a_count}/{self.q_total} correctly'
            self.l_report = ttk.Label(self.frame, text=report, style="Report.TLabel")
            self.l_report.grid(column=0, row=1, columnspan=2, rowspan=3)

            self.frame.pack(expand=True)


if __name__ == '__main__':
    ip_file = 'questions_and_answers.txt'
    question_blocks = open(ip_file).read().rstrip().split('\n\n')
    random.shuffle(question_blocks)

    root = Root(question_blocks)
    root.mainloop()