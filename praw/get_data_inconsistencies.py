import re

files = ['top_comments_2019.txt', 'top_comments_2021.txt']
pat = re.compile(r'''\s(?:[–-]|by)\s
                     |\s\\[–-]\s
                     |\s\*by\s
                     |[,-]\s
                  ''', flags=re.I|re.X)

for file in files:
    print(f'Analyzing "{file}"...')
    with open(file) as f:
        for line in f:
            # Ignore lines containing only whitespaces
            if re.fullmatch(r'\s+', line):
                continue
            elif not pat.search(line):
                print(line, end='')