import re
from rapidfuzz import fuzz

ip_files = ('top_comments_2019.txt', 'top_comments_2021.txt')
op_files = ('top_authors_2019.csv', 'top_authors_2021.csv')

# .* is added at the start so that only the furthest match in the line is extracted
patterns = (r'.*\s(?:[–-]|by)\s+(.+)',
            r'.*\s\\[–-]\s+(.+)',
            r'.*\s\*by\s+(.+)',
            r'.*[,-]\s+(.+)')

fuzzed_2019 = {}
fuzzed_2021 = {}

for ip_file, op_file in zip(ip_files, op_files):
    # Create naive histogram with author name as key and total number of exact matches as the value
    authors = {}
    with open(ip_file) as ipf, open(op_file, 'w') as opf:
        for line in ipf:
            if re.fullmatch(r'\s+', line):
                continue
            for pat in patterns:
                # Quits once the earliest declared match is found
                if m := re.search(pat, line, flags=re.I):
                    name = m[1].strip('*\t ')
                    authors[name] = authors.get(name, 0) + 1
        
        # Merge similar author names
        fuzzed = {}
        # sorted() is used to allow the most popular spelling to win
        for k1 in sorted(authors, key=lambda k: -authors[k]):
            s1 = k1.lower().replace('.', '')
            for k2 in fuzzed:
                s2 = k2.lower().replace('.', '')
                if round(fuzz.ratio(s1, s2)) >= 90:
                    fuzzed[k2] += authors[k1]
                    break
            else:
                fuzzed[k1] = authors[k1]
            
        # Sort fuzzed dictionary by highest votes and write to output files
        opf.write(f'Author,votes\n')
        for name in sorted(fuzzed, key=lambda k: -fuzzed[k]):
            votes = fuzzed[name]
            if votes >= 5:
                opf.write(f'{name},{votes}\n')
    
    if ip_file.find('2021') != -1:
        fuzzed_2021 = fuzzed
    elif ip_file.find('2019') != -1:
        fuzzed_2019 = fuzzed

fuzzed_2019_sorted = sorted(fuzzed_2019, key=lambda k: -fuzzed_2019[k])
fuzzed_2021_sorted = sorted(fuzzed_2021, key=lambda k: -fuzzed_2021[k])

# Display a list of author names who got at least 10 votes in 2021 but less than 5 votes in 2019. 
print('Author who got at least 10 votes in 2021 but less than 5 votes in 2019.\n')
for author_2021 in fuzzed_2021_sorted:
    votes_2021 = fuzzed_2021[author_2021]
    if votes_2021 >= 10:
        for author_2019 in fuzzed_2019_sorted:
            if round(fuzz.ratio(author_2021, author_2019)) >= 90 and fuzzed_2019[author_2019] < 5:
                votes_2019 = fuzzed_2019[author_2019]
                print(f'{author_2021} got {votes_2021} votes in 2021 and {votes_2019} in 2019.\n')
                break


# Find out top-5 authors who had at least 5 votes in both the lists and had the biggest gain in 2021 compared to the 2019 data.
author_vote_gains = {}
for author_2021 in fuzzed_2021_sorted:
    votes_2021 = fuzzed_2021[author_2021]
    if votes_2021 >= 5:
        for author_2019 in fuzzed_2019_sorted:
            if round(fuzz.ratio(author_2021, author_2019)) >= 90 and fuzzed_2019[author_2019] >=5:
                votes_2019 = fuzzed_2019[author_2019]
                author_vote_gains[author_2021] = votes_2021 - votes_2019
                break

print('Top 5 authors who had at least 5 votes in both the lists and had the biggest gain in 2021 compared to the 2019 data.\n')
for author in sorted(author_vote_gains, key=lambda k: -author_vote_gains[k])[:5]:
    print(f'{author} with a gain of {author_vote_gains[author]} votes.\n')
