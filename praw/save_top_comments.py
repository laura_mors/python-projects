import json
#import logging
import praw

# logging.basicConfig(
#     level=logging.INFO, 
#     filename='py_log.log', 
#     filemode='w',
#     format="%(asctime)s %(levelname)s %(message)s")

api_tokens = ''

with open('.api_tokens/api_tokens.json') as f:
    api_tokens = json.load(f)

# Create Reddit instance
reddit = praw.Reddit(
    user_agent=api_tokens['user_agent'],
    client_id=api_tokens['client_id'],
    client_secret=api_tokens['client_secret'],
)

thread_details = (('2019', 'cib77j'), ('2021', 'm20rd1'))

# Create submission objects with the entire url to the submission, or just the id
for year, thread_id in thread_details:
    submission = reddit.submission(id=thread_id)
    # MoreComments objects will be replaced until there are none left
    submission.comments.replace_more(limit=None)

    op_file = f'top_comments_{year}.txt'
    with open(op_file, 'w') as f:
        # Using submission.comments fetches only the top level comments
        for top_level_comment in submission.comments:
           f.write(top_level_comment.body + '\n')